import { Message } from '../message/message';

export class Exchange {
    public id: string;
    public label: string;
    public description: string;
    public messages: Message[];
}
