import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { MaterialModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

import { HomeComponent } from './home.component';

import { Exchange } from '../exchange/exchange';
import { ExchangeService } from '../exchange/exchange.service';

describe ('Home Component', () => {
    let comp: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;
    let exchangeServiceStub: {};

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MaterialModule,
                RouterTestingModule,
            ],
            declarations: [
                HomeComponent,
            ],
            providers: [
                {
                    provide: ExchangeService,
                    useValue: exchangeServiceStub,
                },
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        comp = fixture.componentInstance;
        exchangeServiceStub = {
            getExchanges(): Promise<Exchange[]> {
                const exchanges: Exchange[] = [
                    {
                        id: 'exchange1',
                        label: 'Exchange 1',
                        description: 'First Exchange',
                        messages: [],
                    },
                ];
                return Promise.resolve(exchanges);
            },
        };
    });

    it('should display heading', () => {
        const headingDe: DebugElement = fixture.debugElement.query(By.css('h1'));
        const headingEl: HTMLElement = headingDe.nativeElement;

        expect(headingEl.textContent).toContain('Welcome to Hermes');
    });

    it('should retrieve exchanges', fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(comp.exchanges.length).toBe(1);
        expect(comp.exchanges[0].id).toBe('exchange1');
    }));

    it('should display exchanges', async(() => {
        let cardHeadingDe: DebugElement;
        let cardHeadingEl: HTMLElement;

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            cardHeadingDe = fixture.debugElement.query(By.css('md-card-title'));
            cardHeadingEl = cardHeadingDe.nativeElement;
            expect(cardHeadingEl.textContent).toContain('Exchange 1');
        });
    }));
});
