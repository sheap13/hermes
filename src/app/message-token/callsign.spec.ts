import { Callsign } from './callsign';

describe ('Callsign token', () => {
    let token: Callsign;

    beforeEach(() => {
        token = new Callsign('G-ABCD');
    });

    it ('should read callsign using phonetic spelling', () => {
        expect(token.getSpeechString()).toBe('golf - alpha bravo charlie delta');
    });

    it ('should correctly shorten callsign', () => {
        expect(token.getShortenedDisplayString()).toBe('G-CD');
    });

    it ('should correctly read shortened callsign', () => {
        expect(token.getShortenedSpeechString()).toBe('golf - charlie delta');
    });
});
