export class Message {
    public from: string;
    public message: string;
    public hidden?: boolean;
}
