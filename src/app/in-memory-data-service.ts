import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    public createDb() {
        const exchanges = [
            {
                id: 'radio-check',
                label: 'Radio Check',
                description: `In order to establish that two way communication is possible between the aircraft and the
                ground station, this format can be used. In practice, you may find that it is unnecessary, as at most
                airfields you can satisfactorily confirm that you are able to recieve messages by overhearing others
                communicate while you are getting yourself ready. Your first message to ATC will establish that they are
                able to recieve you too.`,
                messages: [
                    {
                        from: 'Aircraft',
                        message: '*station*, {callsign: callsign}, request radio check on *frequency*.',
                    },
                    {
                        from: 'ATC',
                        message: '{callsign: callsign}, *station*, readability 5.',
                    },
                    {
                        from: 'Aircraft',
                        message: 'Readability 5 also, {callsign: callsign}',
                    },
                ],
            },
        ];
        const users = [
            {
                id: '1',
                callsign: 'G-BSLU',
            },
        ];
        return {
            exchanges,
            users,
        };
    }
}
