import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { MaterialModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

import { ExchangeComponent } from './exchange.component';

import { Exchange } from './exchange';
import { ExchangeService } from './exchange.service';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

describe ('Exchange Component', () => {
    let comp: ExchangeComponent;
    let fixture: ComponentFixture<ExchangeComponent>;
    let exchangeServiceStub: {};
    let userServiceStub: {};

    beforeEach(async(() => {
        exchangeServiceStub = {
            getExchange(id: number): Promise<Exchange> {
                const exchange: Exchange = {
                    id: 'exchange1',
                    label: 'Exchange 1',
                    description: 'First Exchange',
                    messages: [
                        {
                            from: 'Aircraft',
                            message: 'Lorem Ipsum',
                        },
                        {
                            from: 'ATC',
                            message: 'Dolor sit',
                        },
                    ],
                };
                return Promise.resolve(exchange);
            },
        };

        userServiceStub = {
            getUser(id: number): Promise<User> {
                const user: User = {
                    id: '1',
                    callsign: 'G-ABCD',
                };
                return Promise.resolve(user);
            },
        };

        TestBed.configureTestingModule({
            imports: [
                MaterialModule,
                RouterTestingModule,
            ],
            declarations: [
                ExchangeComponent,
            ],
            providers: [
                {
                    provide: ExchangeService,
                    useValue: exchangeServiceStub,
                },
                {
                    provide: UserService,
                    useValue: userServiceStub,
                },
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ExchangeComponent);
        comp = fixture.componentInstance;
    });

    it('should display exchange label', fakeAsync(() => {
        let headingDe: DebugElement;
        let headingEl: HTMLElement;

        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        headingDe = fixture.debugElement.query(By.css('h1'));
        headingEl = headingDe.nativeElement;
        expect(headingEl.textContent).toContain('Exchange 1');
    }));

    it('should display correct number of messages', fakeAsync(() => {
        let cardElements: DebugElement[];
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        cardElements = fixture.debugElement.queryAll(By.css('md-card'));
        expect(cardElements.length).toBe(2);
    }));

    it('should obscure aircraft messages', fakeAsync(() => {
        let cardElements: DebugElement[];
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        cardElements = fixture.debugElement.queryAll(By.css('md-card'));
        expect(cardElements[0].nativeElement.querySelector('md-card-content').textContent).toContain('Message hidden');
    }));

    it('should not obscure ATC messages', fakeAsync(() => {
        let cardElements: DebugElement[];
        let atcCardContent: HTMLElement;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        cardElements = fixture.debugElement.queryAll(By.css('md-card'));
        atcCardContent = cardElements[1].nativeElement.querySelector('md-card-content');
        expect(atcCardContent.textContent).not.toContain('Message hidden');
    }));
});
