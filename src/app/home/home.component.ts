import { Component, OnInit } from '@angular/core';
import { Exchange } from '../exchange/exchange';
import { ExchangeService } from '../exchange/exchange.service';

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html',
})

export class HomeComponent implements OnInit {
    public exchanges: Exchange[] = [];

    constructor(private exchangeService: ExchangeService) {}

    public ngOnInit(): void {
        this.exchangeService.getExchanges()
            .then(result => {
                this.exchanges = result;
            });
    }
}
