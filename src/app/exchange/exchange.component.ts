import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Exchange } from './exchange';
import { ExchangeService } from './exchange.service';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

import { Callsign } from '../message-token/callsign';

import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    selector: 'exchange',
    templateUrl: 'exchange.component.html',
    styleUrls: [
        'exchange.component.css',
    ],
})

export class ExchangeComponent implements OnInit {
    private exchange: Exchange;
    private user: User;
    private callsign: Callsign;

    constructor(
        private exchangeService: ExchangeService,
        private userService: UserService,
        private route: ActivatedRoute,
        private location: Location,
    ) {}

    public ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => {
                return this.exchangeService.getExchange(params['id']);
            })
            .subscribe(exchange => {
                this.exchange = exchange;
                this.exchange.messages.forEach(message => {
                    if (message.from === 'Aircraft') {
                        message.hidden = true;
                    }
                });
                if (this.user != null) {
                    this.detokeniseMessage();
                }
            });

        this.userService.getUser('1')
            .then(user => {
                this.user = user;
                this.callsign = new Callsign(this.user.callsign);
                if (this.exchange != null) {
                    this.detokeniseMessage();
                }
            });
    }

    private detokeniseMessage(): void {
        if (this.exchange != null && this.user != null) {
            this.exchange.messages.forEach(message => {
               message.message = message.message.replace('{callsign: callsign}', this.callsign.getDisplayString());
            });
        } else {
            console.error('attempted to detokenise without requirements');
        }
    }
}
