import { MessageToken } from './message-token';

export class Callsign extends MessageToken {
    private shortenedCallsign: string;
    constructor(text: string) {
        super(text);
        this.shortenedCallsign = this.text.slice(0, 2) + this.text.slice(-2);
    }

    public getSpeechString(): string {
        return this.phoneticise(this.text);
    }

    public getShortenedSpeechString(): string {
        return this.phoneticise(this.shortenedCallsign);
    }

    public getShortenedDisplayString(): string {
        return this.shortenedCallsign;
    }

    private phoneticise(input: string): string {
        return input
            .replace(/A/g, 'alpha ')
            .replace(/B/g, 'bravo ')
            .replace(/C/g, 'charlie ')
            .replace(/D/g, 'delta ')
            .replace(/E/g, 'echo ')
            .replace(/F/g, 'foxtrot ')
            .replace(/G/g, 'golf ')
            .replace(/H/g, 'hotel ')
            .replace(/I/g, 'india ')
            .replace(/J/g, 'juliet ')
            .replace(/K/g, 'kilo ')
            .replace(/L/g, 'lima ')
            .replace(/M/g, 'mike ')
            .replace(/N/g, 'november ')
            .replace(/O/g, 'oscar ')
            .replace(/P/g, 'papa ')
            .replace(/Q/g, 'quebec ')
            .replace(/R/g, 'romeo ')
            .replace(/S/g, 'sierra ')
            .replace(/T/g, 'tango ')
            .replace(/U/g, 'uniform ')
            .replace(/V/g, 'victor ')
            .replace(/W/g, 'whiskey ')
            .replace(/X/g, 'x ray ')
            .replace(/Y/g, 'yankee ')
            .replace(/Z/g, 'zulu ')
            .replace(/-/g, '- ')
            .trim();
    }
}
