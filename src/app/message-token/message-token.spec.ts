import { MessageToken } from './message-token';

describe ('Message token class', () => {
    let token: MessageToken;

    beforeEach(() => {
        token = new MessageToken('test string');
    });

    it ('should return token text as speech response', () => {
        expect(token.getSpeechString()).toBe('test string');
    });

    it ('should return token text as display response', () => {
        expect(token.getDisplayString()).toBe('test string');
    });
});
