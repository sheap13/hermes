import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Exchange } from './exchange';

@Injectable()
export class ExchangeService {
    private exchangesUrl = 'api/exchanges';
    private headers = new Headers({
        'Content-Type': 'application/json',
    });

    constructor(private http: Http) { }

    public getExchanges(): Promise<Exchange[]> {
        return this.http.get(this.exchangesUrl)
            .toPromise()
            .then(response => response.json().data as Exchange[])
            .catch(this.handleError);
    }

   public getExchange(id: string): Promise<Exchange> {
        const url = `${this.exchangesUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as Exchange)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
