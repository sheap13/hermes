import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data-service';

import { ExchangeComponent } from './exchange/exchange.component';
import { ExchangeService } from './exchange/exchange.service';
import { UserService } from './user/user.service';

import { HomeComponent } from './home/home.component';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpModule,
        MaterialModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService),
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        ExchangeComponent,
    ],
    providers: [
        ExchangeService,
        UserService,
    ],
    bootstrap: [
        AppComponent,
    ],
})
export class AppModule { }
