export class MessageToken {
    public id: string;
    protected text: string;

    constructor(text: string) {
        this.text = text;
    }

    public getSpeechString(): string {
        return this.text;
    }
    public getDisplayString(): string {
        return this.text;
    }
}
